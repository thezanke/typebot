(function () {
    setTimeout(function () {
        'use strict';
        
        var newScript, done;
        done = false;
        newScript = document.createElement('script');
        newScript.async = true;
        newScript.src = 'http://typebot.thezanke.com/typebot.js';
        newScript.onload = function () {
            if (!done && (!this.readyState ||
                        this.readyState === "loaded" || this.readyState === "complete")) {
                done = true;
                typebot.run();
            }
        };

        document.getElementsByTagName('body')[0].appendChild(newScript);

    }, 1000);
}());